# vendor_hardmony

   -   [简介](#section01)
   -   [目录结构](#section02)
   -   [各产品信息](#section03)
   -   [使用说明](#section04)
   -   [参与贡献](#section05)
   -   [相关仓](#section06)

## 简介<a name="section01"></a>

该仓库托管hardmony产品：oriole系列OpenHarmony智能硬件。

## 目录结构<a name="section02"></a>

```
vendor_hardmony/
└── oriole          # oriole产品配置
```

oriole：oriole产品配置。当前只有oriole开发者手机一款产品。

## 各产品信息<a name="section03"></a>

以下是各产品介绍，点击产品名可跳转到对应链接：
|产品名     |产品形态      |内容    |
|-------|------|------|
|[oriole](./oriole/README.md)|开发者手机|编译、烧录|

## 使用说明<a name="section04"></a>

见各个子目录README.md

## 参与贡献<a name="section05"></a>

[如何参与](https://gitee.com/openharmony/docs/blob/HEAD/zh-cn/contribute/%E5%8F%82%E4%B8%8E%E8%B4%A1%E7%8C%AE.md)

[Commit message规范](https://gitee.com/openharmony/device_qemu/wikis/Commit%20message%E8%A7%84%E8%8C%83?sort_id=4042860)

## 相关仓<a name="section06"></a>

[device_board_hardmony](https://gitee.com/openharmony-sig/device_board_hardmony/tree/OpenHarmony-sig-5.0/)

[device_soc_spreadtrum](https://gitee.com/openharmony-sig/device_soc_spreadtrum/tree/OpenHarmony-sig-5.0/)
