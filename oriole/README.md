# vendor_hardmony

   -   [简介](#section01)
   -   [目录结构](#section02)
   -   [编译构建方法](#section03)
   -   [烧录方法](#section04)
   -   [使用方法](#section05)
   -   [相关仓](#section06)

## 简介<a name="section01"></a>

oriole是基于展锐uis7885芯片专为开发者设计的手机，搭载了OpenHarmony操作系统，方便开发者进行应用开发和测试。其具备强大的硬件配置和开发工具，以便开发者能够高效地进行应用开发和调试。 

其搭载了紫光展锐的P7855 5G SoC，采用台积电6nm EUV工艺生产，具备高性能和低功耗的特点。该处理器包含1个A76@2.7GHz大核、3个A76@2.3GHz中核和4个A55@2.1GHz小核，以及850MHz的Arm Mali-G57四核GPU。此外，手机配备了8GB内存和128GB存储，支持Nano Sim卡和TF卡拓展，采用6.58英寸FHD分辨率全贴合LCD屏幕，前置8MP镜头，后置64MP主摄和0.3MP微距镜头‌。

![开发者手机外观](./figures/开发者手机形态.png)

图一：开发者手机产品外观

使用场景：基于开发板+孵化仓能够用于2B行业能源领域/金融领域和2B2C手机/平板终端产品北向应用开发。

>[更多详情](https://laval.csdn.net/6512ee3136d6777a12f0fbc0.html)

### 产品特性

|特性       |描述  |
|-----------|----------|
|架构解耦       |1、解耦出原厂闭源能力，留在单板上，其他部分上OH；原厂部分和OH部分都可以独立安装和升级 |
|              | 2、解耦出通用5.15内核 |
|移动通信       |5G/4G/3G/2G,通话、数据、短信  |
|AI             |适配MindSpore Lite和NNRT架构，支持8TOPs NPU  |
|图形性能       |FHD+90FPS、GPU渲染、GSP&DPU硬件合成  |
|摄像头         |前摄、后摄、预览、拍照、录像  |
|音视频         |播放、录音  |
|短距           |WIFI、蓝牙  |
|定位           |Beidou/GPS/Glonass/Galileo，A-GNSS  |
|充电           |充电、快充  |
|传感器         |光感、重力和加速度、地磁传感器、振动  |
|系统应用       |桌面、SystemUI、设置、浏览器、音乐、时钟、计算器、相机  |
|应用开发       |支持应用开发、安装、卸载  |
|安全           |应用签名验证、应用权限管理  |
|电源管理       |支持熄屏、待机、关机，支持电量显示  |
|分布式         |软总线、分布式数据、调度、流转       |
|时间           |支持时间更新                         |
|输入           |触摸输入、按键                       |
|输入法         |支持系统自带输入法                   |
|导航           |三键导航、手势导航                   |
|调试           |HDC调试，Deveco Studio连接调试; 支持查看Hilog、Kmsg日志  |
|可测试性       |支持wukong工具                       |

## 目录结构<a name="section02"></a>

```
vendor/hardmony/oriole/
├── animation                    # 动画资源
├── bundle.json                  # product_oriole组件配置
├── config.json                  # 子系统配置文件
├── default_app_config           # 默认应用配置
├── dm_config                    # display manager配置
├── etc                          # etc配置
├── foundation_cfg               # foundation配置
├── hals                         # 驱动hals配置
├── hdf_config                   # hdf配置
├── http                         # http server服务
├── image_conf                   # 镜像默认配置
├── patch.yml                    # patch管理
├── patch_revert.py              # patch回退脚本
├── power_config                 # 电源管理配置
├── preinstall-config            # 预安装配置
├── product.gni                  # 产品配置
├── resourceschedule             # 资源调度配置
├── security_config              # 安全配置
├── updater_config               # 升级配置
└── wm_config                    # window manager配置
```

animation: 动画资源。可配置充电相关动画。  
battery：电池管理。可配置低电关机电量，如：电量低于2%关机；可配置低电屏幕变暗电量，如：电量低于10%屏幕变暗。    
dm_config：display manager配置。可配置画面旋转角度，如：横竖屏；可配置瀑布屏、曲面屏等特殊屏幕参数。  
hals：驱动的hals层配置。目前只有audio相关配置。     
hdf_config：hdf驱动配置。各个驱动的hcs文件。  
http：http服务启动脚本及配置。   
image_conf:部分镜像文件配置，如:镜像的文件系统类型、分区大小等。  
patch.yml:配置了打patch所需要的，源文件路径及目标文件路径，--patch命令会解析此文件。   
patch_revert.py:patch回退脚本。  
preinstall-config:应用预安装配置文件。配置了如：相册、launcher、systemui等系统应用。   
resourceschedule：资源调度配置。包含soc性能相关配置，如：soc性能提升、soc资源分配。  
security_config：安全配置。可以配置高优先级进程、严重重启进程。   
updater_config:升级配置。  
wm_config:window manager配置。可配置应用窗口及动画效果，如：动画时间、平移效果、缩放效果等。 

## 编译构建方法<a name="section03"></a>

### 环境配置
参考官方 (https://docs.openharmony.cn/pages/v5.0/zh-cn/device-dev/subsystems/subsys-build-all.md#环境配置)。

### 获取源码

开发者手机上OH社区的主干工程，工程指向OH5.0.0 Release tag版本。

该工程不包含展锐代码或二进制文件，展锐相关镜像将预置到开发者手机上。

```
repo init -u https://gitee.com/openharmony-sig/manifest -m devboard_developphone2_5.0.xml
repo sync -c
repo forall -c 'git lfs pull'
```

确认源码获取成功，执行预编译命令下载编译工具：
```
./build/prebuilts_download.sh
```

### 编译oriole产品

主要介绍本工程如何编译oriole产品。

1. 首次编译：

   编译前请先手动注释 build/hb/util/system_util.py 第40、41行
   ```
              if '' in cmd:
                  cmd.remove('')
   ```

   首次编译需要打patch，添加编译参数--patch，编译命令：   
   ./build.sh --product-name oriole --ccache --patch    

   > 注1 编译命令支持选项参考：(https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-build-all.md#编译命令)   
   > 注2 patch配置见： vendor_hardmony/oriole/patch.yml 文件。  
   > 注3 撤消patch方法：
   >如需撤销patch.yml中所有patch，执行vendor/hardmony/oriole下的patch_revert.py脚本，如：
   >```
   >   cd vendor/hardmony/oriole
   >   python3 patch_revert.py
   >```
   >也可手动撤消单个patch，例如取消build仓patch，可在build目录下执行
   >```
   >patch -p1 -R < ../device/soc/spreadtrum/patches/build/0001-add-soc_platform.patch
   >```

2. 非首次编译：跳过打patch环节，去掉编译参数--patch，执行命令：

   ```
   ./build.sh --product-name oriole –-ccache
   ```

>注：当前编译出的镜像只包含system.img、vendor.img等OH相关开源镜像，不是最终可烧录的pac包，生成可烧录pac包需要进行下一步打包操作。

### 打包操作

打包操作：[开发者手机打包指导(OH 4.1&5.0)](https://laval.csdn.net/674d715f522b003a54742fea.html?v=2)


## 烧录方法<a name="section04"></a>

本章介绍如何使用UpgradeDownload.exe工具，烧录新的镜像。

### 环境准备

1. 工具下载

    下载地址： https://pan.baidu.com/s/1UniJM5Z6F-8TOUIHR-Cd9g  
    提取码：f3uq
    >此链接包含驱动以及烧录程序，使用烧录程序前需要安装驱动。

2. 工具安装

    驱动安装，根据系统类型进入不同目录，进入后双击DriverSetup.exe：  
    ![驱动安装](./figures/驱动安装.png)   
    运行烧录软件，双击UpgradeDownload.exe：  
    ![运行烧录软件](./figures/运行烧录软件.png)

### 镜像烧录

>获取历史完整镜像：[开发者手机镜像包（OH 5.0 ）](https://laval.csdn.net/674535e6522b003a54704cf5.html?login=from_csdn)，此镜像可直接烧录。
    
1. 加载文件

    打开下载工具，单击工具栏设置图标![配置pac包](./figures/配置pac包.png)。   
    选择需要烧录的文件包：
    ![选择pac包](./figures/选择pac包.png)

2. 开始烧录

    手机关机，不插入数据线，单击工具栏图标![烧录按键](./figures/烧录按键.png)。  
    按住手机音量加、减以及电源键，插入数据线，等待几秒钟进入烧录模式，开始烧录后松开按键，如下图所示：
    ![开始烧录](./figures/开始烧录.png)

3. 烧录完成

    烧录完成后，软件将显示如下内容：   
    ![烧录完成](./figures/烧录完成.png)

4. 退出烧录

    单击图标![停止烧录](./figures/停止烧录.png)，可退出自动下载状态。   
    这时工具栏其他按钮变成可用状态，可以点击![配置pac包](./figures/配置pac包.png)重新加载其他文件。

## 使用方法<a name="section05"></a>

各模块适配方法，请参考：    
[启动适配](https://gitee.com/openharmony-sig/device_soc_spreadtrum/blob/OpenHarmony-sig-5.0/doc/Startup.md)

[Display适配](https://gitee.com/openharmony-sig/device_soc_spreadtrum/blob/OpenHarmony-sig-5.0/doc/Display.md)

[Screen适配](https://gitee.com/openharmony-sig/device_soc_spreadtrum/blob/OpenHarmony-sig-5.0/doc/Screen.md)

[NPU适配](https://gitee.com/openharmony-sig/device_soc_spreadtrum/blob/OpenHarmony-sig-5.0/doc/Nnrt.md)

[Modem适配](https://gitee.com/openharmony-sig/device_soc_spreadtrum/blob/OpenHarmony-sig-5.0/doc/Modem.md)

[BT框架适配](https://gitee.com/openharmony-sig/device_soc_spreadtrum/blob/OpenHarmony-sig-5.0/doc/BT.md)

[Wifi适配](https://gitee.com/openharmony-sig/device_soc_spreadtrum/blob/OpenHarmony-sig-5.0/common/wpa_supplicant/README.md)

[Audio适配](https://gitee.com/openharmony-sig/device_soc_spreadtrum/blob/OpenHarmony-sig-5.0/doc/Audio.md)

[Gnss适配](https://gitee.com/openharmony-sig/device_soc_spreadtrum/blob/OpenHarmony-sig-5.0/doc/Gnss.md)    

## 相关仓<a name="section06"></a>

[device_board_hardmony](https://gitee.com/openharmony-sig/device_board_hardmony/tree/OpenHarmony-sig-5.0/)

[device_soc_spreadtrum](https://gitee.com/openharmony-sig/device_soc_spreadtrum/tree/OpenHarmony-sig-5.0/)
